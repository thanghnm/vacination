//Khai báo mongoose
const mongoose = require("mongoose")
// Khai báo schema
const Schema = mongoose.Schema
// Khởi tạo schema
const contactSchema = new Schema({
    _id:  mongoose.Types.ObjectId,
    email:{
        type:String,
        required:true
    },
    
},
{timestamps:true})
// Biên dịch Schema
module.exports = mongoose.model("Contact",contactSchema)