// Import Thư viện Express
const express = require("express");
const path = require('path');
const { getTime, getUrl } = require("../middlewares/index.middleware");
// Khai báo router
const router = express.Router();
// Router 
router.get("/", getTime,getUrl,(req, res) => {
    res.sendFile(path.join(__dirname + "../../../frontEnd/Vacination/Vacination.ng.html"))
})
// Router 
router.get("/admin/users/",(req, res) => {
    res.sendFile(path.join(__dirname + "../../../frontEnd/admin/user/user.html"))
})
// Router 
router.get("/admin/contacts/",(req, res) => {
    res.sendFile(path.join(__dirname + "../../../frontEnd/admin/contact/contact.html"))
})

// Export
module.exports = router
