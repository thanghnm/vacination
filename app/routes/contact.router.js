// Import Thư viện Express
const express = require("express");
const { createContact,  getAllContact, getContactById, updateContactById, deleteContactById } = require("../controllers/contact.controller");
// Khai báo router
const router = express.Router();

// Router lấy danh sách contact
router.get("/", getAllContact)
// Router lấy contact bằng id
router.get("/:contactId/",getContactById )
// Router tạo contact
router.post("/", createContact)
// Router update contact bằng id
router.put("/:contactId/",updateContactById )
// // Router delete contact bằng id
router.delete("/:contactId/",deleteContactById )
// Export
module.exports = router
