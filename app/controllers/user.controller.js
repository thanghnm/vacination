//Import Model
const userModel = require("../models/user.model")
const mongoose = require("mongoose")
// Hàm tạo User
const createUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { fullName, status, phone } = req.body
    // B2 Kiểm tra dữ liệu
    if (!fullName) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required"
        })
    }
    if (!phone) {
        return res.status(400).json({
            status: "Bad request",
            message: "phone is required"
        })
    }
    // B3 Xử lý 
    let newUser = {
        _id: new mongoose.Types.ObjectId(),
        fullName,
        status,
        phone
    }
    try {
        const result = await userModel.create(newUser);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

    /* Chasing
    {
        "result": {
        "_id": "6522399d6dd3218d94b37818",
        "fullName": "tui",
        "phone": "0909090909",
        "status": "Level 0",
        "__v": 0
    }
    */
}
// Hàm Lấy dữ liệu tất cả User
const getAllUser = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.find();
    return res.status(200).json({
        result
    });

}
// Hàm lấy dữ liệu Drink bằng Id
const getUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await userModel.findById(userId);
    return res.status(200).json({
        result
    });
    /* Chasing
{
"result": {
    "_id": "6522399d6dd3218d94b37818",
    "fullName": "tui",
    "phone": "0909090909",
    "status": "Level 0",
    "__v": 0
}
}
*/
}
// Hàm Update User bằng Id
const updateUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    const { fullName, phone, status } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(userId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedUser = {
        fullName,
        status,
        phone
    }
    const result = await userModel.findByIdAndUpdate(userId, updatedUser);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });
    /* Chasing
   "status": "Update User successfully",
    "result": {
        "_id": "6522399d6dd3218d94b37818",
        "fullName": "tui",
        "phone": "0909090909",
        "status": "Level 0",
        "__v": 0
    }      
    */
}
// Hàm xóa User bằng Id
const deleteUserById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var userId = req.params.userId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await userModel.findByIdAndDelete(userId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = { createUser  ,getAllUser, getUserById, updateUserById,deleteUserById
}