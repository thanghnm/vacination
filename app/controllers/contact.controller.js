//Import Model
const contactModel = require("../models/contact.model")
const mongoose = require("mongoose")
// Hàm tạo Contact
const createContact = async (req, res) => {
    // B1 Thu thập dữ liệu
    const { email } = req.body
    // B2 Kiểm tra dữ liệu
    if (!email) {
        return res.status(400).json({
            status: "Bad request",
            message: "fullName is required"
        })
    }
    
    // B3 Xử lý 
    let newContact = {
        _id: new mongoose.Types.ObjectId(),
        email
    }
    try {
        const result = await contactModel.create(newContact);
        return res.status(201).json({
            result
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal Server Error",
            message: error.message
        })
    }

}
// Hàm Lấy dữ liệu tất cả Contact
const getAllContact = async (req, res) => {
    // B1 Thu thập dữ liệu
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await contactModel.find();
    return res.status(200).json({
        result
    });

}
// Hàm lấy dữ liệu Contact bằng Id
const getContactById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var contactId = req.params.contactId
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "User Id is invalid"
        })
    }
    // B3 Xử lý 
    const result = await contactModel.findById(contactId);
    return res.status(200).json({
        result
    });

}
// Hàm Update Contact bằng Id
const updateContactById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var contactId = req.params.contactId
    const { email } = req.body
    // B2 Kiểm tra dữ liệu
    if (!mongoose.Types.ObjectId.isValid(contactId)) {
        return res.status(400).json({
            status: "Bad request",
            message: "userId is invalid"
        })
    }
    // B3 Xử lý 
    let updatedContact = {
        email
    }
    const result = await contactModel.findByIdAndUpdate(contactId, updatedContact);
    return res.status(200).json({
        status: "Update User successfully",
        result
    });

}
// Hàm xóa Contact bằng Id
const deleteContactById = async (req, res) => {
    // B1 Thu thập dữ liệu
    var contactId = req.params.contactId
    // B2 Kiểm tra dữ liệu
    // B3 Xử lý 
    const result = await contactModel.findByIdAndDelete(contactId);
    return res.status(204).json({
        status: "Delete User successfully"
    });
}
// Export
module.exports = { createContact, getAllContact, getContactById, updateContactById, deleteContactById
}